package com.mediatek.drivercontrol;

import android.util.Log;

public class drivercontrol {
	private static final String TAG = "drivercontrol";
	private static final boolean DEBUG = false;

	static {
		try {
			System.loadLibrary("drivercontrol_jni");
		} catch (UnsatisfiedLinkError ule) {
			Log.d(TAG, ule.getMessage());
		}

	}
	// for led control

	// red led on / off
	public static native void gm_led_RED_onoff(int onoff); // 1 for on , 0 for
															// off
	// red led flash fast 500ms

	public static native void gm_led_RED_flash_fast();

	// red led flash slowly 1s
	public static native void gm_led_RED_flash_slowly();

	// blue led on / off
	public static native void gm_led_BLUE_onoff(int onoff); // 1 for on , 0 for
															// off
	// blue led flash fast 500ms

	public static native void gm_led_BLUE_flash_fast();

	// blue led flash slowly 1s
	public static native void gm_led_BLUE_flash_slowly();

	// green led on / off
	public static native void gm_led_GREEN_onoff(int onoff); // 1 for on , 0 for
																// off
	// green led flash fast 500ms

	public static native void gm_led_GREEN_flash_fast();

	// green led flash slowly 1s
	public static native void gm_led_GREEN_flash_slowly();

	// all leds off
	public static native void gm_led_pattern_off();

	// the device have no Infrared lamp , ignore this
	public static native void gm_led_IRDA_led_full_on();

	// the device have no Infrared lamp , ignore this
	public static native void gm_led_IRDA_led_half_on();

	// the device have no Infrared lamp , ignore this
	public static native void gm_led_IRDA_led_off();

	// enable PIR motion sensor infrared induction
	public static native void gm_led_Infrared_induction_on();

	// disabel PIR motion sensor infrared induction
	public static native void gm_led_Infrared_induction_off();

	// the device have no Infrared lamp , ignore this
	public static native void gm_led_IRDA_led_enable();

	// the device have no Infrared lamp , ignore this
	public static native void gm_led_IRDA_led_disable();

	public static native void gm_camera_band_set(int band); // 0: 50hz 1: 60hz

	public static native void gm_camera_flip_set(int flip); // 0: normal 1: h
															// mirror 2:v mirror
															// 3:hv mirror

}
