package com.xvidia.obd.reader.data;
public class ObdPressureData {
	String sensorId = "FUEL_PRESSURE_OBD";
	String deviceId = null;
	String tripId = null;
	String carRegNo = null;
	float fuelPressure;
	long timeStamp = 0;
	String id=null;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public float getFuelPressure() {
		return fuelPressure;
	}

	public void setFuelPressure(float fuelPressure) {
		this.fuelPressure = fuelPressure;
	}

	public String getCarRegNo() {
		return carRegNo;
	}

	public void setCarRegNo(String carRegNo) {
		this.carRegNo = carRegNo;
	}


	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	

	
	
}
