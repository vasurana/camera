package com.xvidia.cabi.service;

/**
 * Created by vasu on 22/12/15.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.xvidia.cabi.R;
import com.xvidia.cabi.broadcastReceiver.NetworkStateReceiver;
import com.xvidia.cabi.data.LogData;
import com.xvidia.cabi.fragments.MeterView;
import com.xvidia.cabi.network.IAPIConstants;
import com.xvidia.cabi.network.ServiceURLManager;
import com.xvidia.cabi.network.VolleySingleton;
import com.xvidia.cabi.network.model.CreateSession;
import com.xvidia.cabi.utils.ActivityTypes;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.mediatek.drivercontrol.drivercontrol;

public class PublishLiveStreamActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener, Session.SessionListener,
        PublisherKit.PublisherListener, Session.ArchiveListener, Session.SignalListener {

    private static final String LOG_TAG = PublishLiveStreamActivity.class.getSimpleName();

    //    private Button toggleVoice;
    private String mApiKey;
    private String mSessionId;
    private String mStatus;
    private String mToken;
    private Session mSession;
    private Publisher mPublisher;
    private String mCurrentArchiveId;
    private FrameLayout mPublisherViewContainer;
    private ProgressDialog pDialog;
    private ImageView cancelSession;
    private LocationManager locationManager = null;
    private Criteria criteria;
    private String provider;
    static String trip_id = "tripId";
    static String regNo = "regNo";
    static int TIMER_TIME_2SEC = 2 * 1000;
    private NetworkStateReceiver networkStateReceiver;
    private boolean firstFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publisher_live);
        cancelSession = (ImageView) findViewById(R.id.cancelLiveSession);
        cancelSession.setImageResource(R.drawable.cross_white);
        cancelSession.setVisibility(View.GONE);
        cancelSession.setOnClickListener(new View.OnClickListener(

        ) {
            @Override
            public void onClick(View v) {
                callSessionCancel();
            }
        });
        mPublisherViewContainer = (FrameLayout) findViewById(R.id.publisher_container);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Connecting...Please wait.");
        firstFlag = true;
        pDialog.setCancelable(true);
        showDialog();
        fetchPublishSessionConnectionData();
        mPublisherViewContainer.setVisibility(View.VISIBLE);

    }


    private void callSessionCancel() {
        if (mSession != null)
            mSession.disconnect();
        finish();
    }


    public void flipCamera(View v) {

        mPublisherViewContainer.removeView(mPublisher.getView());
        mPublisher.swapCamera();
        mPublisherViewContainer.addView(mPublisher.getView());

    }

    private void initializeSession() {
        mSession = new Session(this, mApiKey, mSessionId);
        mSession.setSessionListener(this);
        mSession.setArchiveListener(this);
        mSession.setSignalListener(this);
        mSession.connect(mToken);
    }

    private void initializePublisher() {
        mPublisher = new Publisher(getApplicationContext(), "Xvidia's Demo", true, true);
        mPublisher.setPublisherListener(this);
        mPublisher.getRenderer().setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE,
                BaseVideoRenderer.STYLE_VIDEO_FILL);
        mPublisher.setPublishAudio(true);
        mPublisherViewContainer.addView(mPublisher.getView(), 0);
        mPublisherViewContainer.setVisibility(View.VISIBLE);
        hideDialog();

    }

    private void logOpenTokError(OpentokError opentokError) {
        Log.e(LOG_TAG, "Error Domain: " + opentokError.getErrorDomain().name());
        Log.e(LOG_TAG, "Error Code: " + opentokError.getErrorCode().name());
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    @Override
    public void onBackPressed() {

//        restartAudioMode();
//        if (mSession != null)
//            mSession.disconnect();
//        super.onBackPressed();
//        finish();
    }

//    /**
//     * In this method Location services is initialised
//     */
//    private void initializeLocationManager() {
//        // Get the location manager
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        // Define the criteria how to select the location provider
//        criteria = new Criteria();
//        criteria.setAccuracy(Criteria.ACCURACY_COARSE);    //default
//
//        criteria.setCostAllowed(false);
//        // get the best provider depending on the criteria
//        provider = locationManager.getBestProvider(criteria, false);
//
//        // the last known location of this provider
//        Location location = locationManager.getLastKnownLocation(provider);
//
//
//        if (location != null) {
//            onLocationChanged(location);
//        } else {
//            // leads to the settings because there is no last known location
////		  Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
////		  startActivity(intent);
//        }
//        locationManager.requestLocationUpdates(provider, TIMER_TIME_2SEC, 1, this);
//        // location updates: at least 1 meter and 200millsecs change
//    }
//    /**
//     * Checks internet connectivity
//     *
//     * @return boolean true/false
//     */
//
//    boolean checkNetwrk() {
//        boolean nwFlag = false;
//        try {
//            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//            if (networkInfo != null && networkInfo.isConnected()) {
//                nwFlag = true;
//            }
//
//        } catch (Exception e) {
//        }
//
//        return nwFlag;
//    }
//
//    /**
//     * Sends http request to server
//     *
//     * @param jsonRequest
//     * @param api
//     */
//    private void sendPostRequest(String jsonRequest, int api) {
//        if (jsonRequest != null) {
//
//            if (checkNetwrk()) {
//                final String url = ServiceURLManager.getInstance().getUrl(api);
//
//                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.i("Volley JSONObject ", response.toString());
//                        } catch (Exception e) {
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        try {
//                            if (error != null) {
//                                NetworkResponse networkResponse = error.networkResponse;
//                                Log.i("Volley error service ", error.getMessage());
//                            }
//                        }catch (Exception e){
//
//                        }
//                    }
//                });
//                VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);
//            }
//        }
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        float lat = (float) location.getLatitude();
//        float longitude = (float) location.getLongitude();
//        float altitude = (float) location.getAltitude();
//        float speed = location.getSpeed();
//        Date nw = new Date();
//        long time = nw.getTime();
//
//
//        Log.i("onLocation Service", "onLocationChanged " + speed);
//
////        sendLocationRequest(time, lat, longitude, altitude, speed);
//
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }
//
//    /**
//     * Sends Location data
//     *
//     * @param timeStamp
//     * @param latitude
//     * @param longitude
//     * @param altitude
//     * @param speed
//     */
//    public void sendLocationRequest(long timeStamp, float latitude, float longitude, float altitude, float speed) {
//        String android_id = LogData.getInstance().getDeviceId(MyApplication.getAppContext());
//        JSONObject jsonRequest = null;
//        ObjectMapper mapper = new ObjectMapper();
//        String jsonRequestString = null;
//        LocationData locationData = new LocationData();
//        locationData.setSensorId("GPS");
//        locationData.setId(""+timeStamp);
//        locationData.setDeviceId(android_id);
//        locationData.setCarRegNo(regNo);
//        locationData.setTripId(trip_id);
//        locationData.setLat(latitude);
//        locationData.setLongitude(longitude);
//        locationData.setAltitude(altitude);
//        locationData.setSpeed(speed);
//        locationData.setTimeStamp(timeStamp);
//        try {
//            jsonRequestString = mapper.writeValueAsString(locationData);
//            Log.i("VolleyLayoutTimeData ", jsonRequestString.toString());
//        } catch (IOException e) {
//        }
//
//        sendPostRequest(jsonRequestString, IAPIConstants.API_KEY_LOCATION);
//    }

    public void fetchPublishSessionConnectionData() {
        try {

            String subUrl = ServiceURLManager.getInstance().getUrl(IAPIConstants.API_KEY_GET_PUBLISHER);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, subUrl, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {

                        mSessionId = response.getString("sessionId");
                        mToken = response.getString("token");
                        mApiKey = response.getString("apiKey");
                        mStatus = response.getString("status");
                        Log.i("mSessionId", mSessionId);
                        Log.i("mToken", mToken);
                        Log.i("mApiKey", mApiKey);
                        Log.i("mStatus", mStatus);

                        if (mStatus != null && mStatus.equals(ActivityTypes.LIVE_CREATED)) {
                            initializeSession();
                            initializePublisher();
                            firstFlag = false;
                            callUpdateServer();

                        } else {
                            showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                        }
                        Log.i(LOG_TAG, mApiKey);
                        Log.i(LOG_TAG, mSessionId);
                        Log.i(LOG_TAG, mToken);


                    } catch (Exception e) {
                        hideDialog();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    showMessage(getString(R.string.error_live_session_closed), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    //add params <key,value>
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    // add headers <key,value>
                    String credentials = "user" + ":" + "govind";
                    String auth = "Basic "
                            + Base64.encodeToString(credentials.getBytes(),
                            Base64.NO_WRAP);
                    headers.put("Authorization", auth);
                    return headers;
                }

            };
            VolleySingleton.getInstance(this).addToRequestQueue(request);

            new Handler().postDelayed(new Runnable() {

                public void run() {
                    startService(new Intent(PublishLiveStreamActivity.this, LocationService.class));
                }
            }, 3000);
        } catch (Exception e) {

        }
    }


    private void callUpdateServer() {
        try {
            String url = ServiceURLManager.getInstance().sendSessionDetailsUrl();
            CreateSession createSession = new CreateSession();
            String carRegNo = LogData.getInstance().getCarRegNoKey(PublishLiveStreamActivity.this);
            createSession.setApiKey(mApiKey);
            createSession.setCarRegNo(carRegNo);
            createSession.setSessionId(mSessionId);
            createSession.setToken(mToken);

            ObjectMapper mapper = new ObjectMapper();
            String jsonObject = null;
            try {
                jsonObject = mapper.writeValueAsString(createSession);
            } catch (IOException e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("response", error.toString());
                    showError(getString(R.string.error_general), null);
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//            final JsonObjectRequest requestFinal = request;
            VolleySingleton.getInstance(MyApplication.getAppContext()).addToRequestQueue(request);//			}

        } catch (Exception e) {
            e.printStackTrace();

        }


    }

    /* Session Listener methods */

    @Override
    public void onConnected(Session session) {
        Log.i(LOG_TAG, "Session Connected");

        if (mPublisher != null) {
            mSession.publish(mPublisher);
            hideDialog();
//            messageLayout.setVisibility(View.VISIBLE);
            // Initialize publisher meter view
            final MeterView meterView = (MeterView) findViewById(R.id.publishermeter);
            meterView.setIcons(BitmapFactory.decodeResource(getResources(),
                    R.drawable.unmute_pub), BitmapFactory.decodeResource(
                    getResources(), R.drawable.mute_pub));
            mPublisher.setAudioLevelListener(new PublisherKit.AudioLevelListener() {
                @Override
                public void onAudioLevelUpdated(PublisherKit publisher,
                                                float audioLevel) {
                    meterView.setMeterValue(audioLevel);
                }
            });
            meterView.setOnClickListener(new MeterView.OnClickListener() {
                @Override
                public void onClick(MeterView view) {
                    mPublisher.setPublishAudio(!view.isMuted());
                }
            });
        }

//        mSession.sendSignal(SIGNAL_TYPE_CHAT, "Vasu Hiii");
    }

    @Override
    public void onDisconnected(Session session) {
        Log.i(LOG_TAG, "Session Disconnected");
//        closePublishSessionConnection();
       /* showMessage("Session Disconnected", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });*/
        mPublisher.destroy();

    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Received");
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Dropped");

    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        logOpenTokError(opentokError);
    }

    /* Publisher Listener methods */

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        Log.i(LOG_TAG, "Publisher Stream Created");
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
        Log.i(LOG_TAG, "Publisher Stream Destroyed");
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        logOpenTokError(opentokError);
    }

    /* Archive Listener methods */
    @Override
    public void onArchiveStarted(Session session, String archiveId, String archiveName) {

        mCurrentArchiveId = archiveId;
        Log.i(LOG_TAG, mCurrentArchiveId + "mCurrentArchiveId");

    }

    @Override
    public void onArchiveStopped(Session session, String archiveId) {


    }


    /* Signal Listener method */
    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {
//        boolean remote = !connection.equals(mSession.getConnection());
//        switch (type) {
//            case SIGNAL_TYPE_CHAT:
//                showMessage(data, remote);
//                break;
//
//            case SIGNAL_TYPE_IMAGE:
//                heartImageView.setVisibility(View.VISIBLE);
//                heartImageView.setImageResource(R.drawable.heart_selected);
//                heartImageView.startAnimation(pulse);
//                pulse.setAnimationListener(new Animation.AnimationListener() {
//
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        heartImageView.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//
//        }
    }

    private void showDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!pDialog.isShowing())
                    pDialog.show();
            }
        });
    }

    private void hideDialog() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        networkStateReceiver = new NetworkStateReceiver(this);
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(networkStateReceiver!=null) {
            networkStateReceiver.removeListener(this);
            this.unregisterReceiver(networkStateReceiver);
        }
        drivercontrol.gm_led_BLUE_onoff(0);
        drivercontrol.gm_led_GREEN_onoff(0);
        drivercontrol.gm_led_RED_onoff(0);
        drivercontrol.gm_led_RED_flash_fast();

        callSessionCancel();
    }

    private void showMessage(String mesg, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(mesg)
                .setPositiveButton(android.R.string.ok, okClicked)
                .setCancelable(false)
                .show();
    }

    private void showError(String message, DialogInterface.OnClickListener okClicked) {
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, okClicked)
                .show();
    }

    @Override
    public void onNetworkAvailable() {

        if (!firstFlag) {
            if (mPublisher != null) {
                if (mPublisherViewContainer != null)
                    mPublisherViewContainer.removeView(mPublisher.getView());
                if (mSession != null)
                    mSession.disconnect();
                if (mPublisher != null)
                    mPublisher.destroy();
                mSession = null;
                mPublisher = null;
            }
            initializeSession();
            initializePublisher();
        }
        checkNetwrk();
    }

    @Override
    public void onNetworkUnavailable() {

        Log.e("OnUnAvailable", "UnAvailable");
        drivercontrol.gm_led_RED_onoff(0);
        drivercontrol.gm_led_BLUE_onoff(0);
        drivercontrol.gm_led_GREEN_onoff(0);
        drivercontrol.gm_led_BLUE_flash_fast();


    }

    void checkNetwrk(){
        try{
            drivercontrol.gm_led_BLUE_onoff(0);
            drivercontrol.gm_led_GREEN_onoff(0);
            drivercontrol.gm_led_RED_onoff(0);
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();

            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    drivercontrol.gm_led_BLUE_onoff(1);

                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    // connected to the mobile provider's data plan
                    drivercontrol.gm_led_GREEN_onoff(1);
                }
            } else {
                // not connected to the internet
                drivercontrol.gm_led_BLUE_flash_fast();
            }

        }catch (Exception e) {
        }

    }
}
