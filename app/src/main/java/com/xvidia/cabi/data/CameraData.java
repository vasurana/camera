package com.xvidia.cabi.data;



public class CameraData{
	String id = null;
	String deviceId = null;
	String assetId = null;
	String deviceName = null;
	String car = null;
	String accelerometerReadingList = null;
	String ambientTemperatureList = null;
	String gpsList = null;
	String gyroscopeList = null;
	String lightList = null;
	String pressureList = null;
	String proximityList = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	public String getAccelerometerReadingList() {
		return accelerometerReadingList;
	}
	public void setAccelerometerReadingList(String accelerometerReadingList) {
		this.accelerometerReadingList = accelerometerReadingList;
	}
	public String getAmbientTemperatureList() {
		return ambientTemperatureList;
	}
	public void setAmbientTemperatureList(String ambientTemperatureList) {
		this.ambientTemperatureList = ambientTemperatureList;
	}
	public String getGpsList() {
		return gpsList;
	}
	public void setGpsList(String gpsList) {
		this.gpsList = gpsList;
	}
	public String getGyroscopeList() {
		return gyroscopeList;
	}
	public void setGyroscopeList(String gyroscopeList) {
		this.gyroscopeList = gyroscopeList;
	}
	public String getLightList() {
		return lightList;
	}
	public void setLightList(String lightList) {
		this.lightList = lightList;
	}
	public String getPressureList() {
		return pressureList;
	}
	public void setPressureList(String pressureList) {
		this.pressureList = pressureList;
	}
	public String getProximityList() {
		return proximityList;
	}
	public void setProximityList(String proximityList) {
		this.proximityList = proximityList;
	}
	public String getHeartBeatList() {
		return heartBeatList;
	}
	public void setHeartBeatList(String heartBeatList) {
		this.heartBeatList = heartBeatList;
	}
	String heartBeatList = null;
	

	
	
}
