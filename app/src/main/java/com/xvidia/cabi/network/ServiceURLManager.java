package com.xvidia.cabi.network;


public class ServiceURLManager implements IAPIConstants {

    private static ServiceURLManager instance = null;

    private ServiceURLManager() {

    }

    public static ServiceURLManager getInstance() {
        if (instance == null) {
            instance = new ServiceURLManager();
        }
        return instance;
    }
    final String base_URL_LIVE = "http://52.73.0.203/opentokproxy";//Amazon//
    final String base_URL_OBD = "http://54.85.7.217:8999";//Amazon//
    final String base_URL_VIDEO = "http://54.85.7.217:9090";
	final String base_URL ="http://52.87.117.228:9090";//
//    final String base_URL ="http://192.168.1.13:9090";//

    private String getURL(int SERVICE_API) {
        String service = "";
        switch (SERVICE_API) {
            case API_KEY_DEVICE_INIT:
                service = base_URL + ServiceURL.API_KEY_DEVICE_INIT;
                break;
            case API_KEY_REGISTER_DEVICE:
                service = base_URL + ServiceURL.API_KEY_REGISTER_DEVICE;
                break;
            case API_KEY_UPLOAD_VIDEO:
                service = base_URL_VIDEO + ServiceURL.API_KEY_UPLOAD_VIDEO;
                break;
            case API_KEY_PRE_REG_DEMO:
                service = base_URL + ServiceURL.API_KEY_PRE_REG_DEMO;
                break;
            case API_KEY_OBD_COOLANT_TEMPERATURE:
                service = base_URL_OBD + ServiceURL.API_KEY_OBD_COOLANT_TEMPERATURE;
                break;
            case API_KEY_OBD_SPEED:
                service = base_URL_OBD + ServiceURL.API_KEY_OBD_SPEED;
                break;
            case API_KEY_OBD_PRESSURE:
                service = base_URL_OBD + ServiceURL.API_KEY_OBD_PRESSURE;
                break;
            case API_KEY_OBD_THROTTLE:
                service = base_URL_OBD + ServiceURL.API_KEY_OBD_THROTTLE;
                break;
            case API_KEY_OBD_RPM:
                service = base_URL_OBD + ServiceURL.API_KEY_OBD_RPM;
                break;
            case API_KEY_OBD_FUELTYPE:
                service = base_URL_OBD + ServiceURL.API_KEY_OBD_FUELTYPE;
                break;
            case API_KEY_LOCATION:
                service = base_URL + ServiceURL.API_KEY_LOCATION;
                break;
            case API_KEY_GET_PUBLISHER:
                service = base_URL_LIVE + ServiceURL.API_KEY_GET_PUBLISHER;
                break;
            case API_KEY_GET_SUBSCRIBER:
                service = base_URL + ServiceURL.API_KEY_GET_SUBSCRIBER;
                break;
            case API_KEY_UPDATE_LIVE_API_KEY:
                service = base_URL + ServiceURL.API_KEY_UPDATE_LIVE_API_KEY;
                break;
        }
        return service;
    }


    public String getUrl(int urlKey) {
        return getURL(urlKey);
    }

    public String getLiveSubscriberTokenUrl(String sessionId) {
        String url = getURL(API_KEY_GET_SUBSCRIBER) + sessionId;
        url = url.replace("+", "%2B");
        return url;
    }

    public String getCloseLiveUrl(String sessionId) {
        String url = getURL(API_KEY_LIVE_CLOSE) + sessionId;
        url = url.replace("+","%2B");
        return url;
    }
    public String sendSessionDetailsUrl() {
        String url = getUrl(API_KEY_UPDATE_LIVE_API_KEY);
        url = url.replace("+","%2B");
        return url;
    }


    public String getUploadVideoUrl() {
        return getURL(API_KEY_UPLOAD_VIDEO);
    }


}
