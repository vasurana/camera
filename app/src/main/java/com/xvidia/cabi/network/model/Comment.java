package com.xvidia.cabi.network.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment {

	private String uuid;
	private String comment;
	private long date;
	private String profile;
	private String commenter; //username
	private String commenterDisplayName;
	private String commenterThumbNail;
	private String momentUuid;
	private String momentOwner; //username

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getCommenter() {
		return commenter;
	}

	public void setCommenter(String commenter) {
		this.commenter = commenter;
	}

	public String getCommenterDisplayName() {
		return commenterDisplayName;
	}

	public void setCommenterDisplayName(String commenterDisplayName) {
		this.commenterDisplayName = commenterDisplayName;
	}

	public String getCommenterThumbNail() {
		return commenterThumbNail;
	}

	public void setCommenterThumbNail(String commenterThumbNail) {
		this.commenterThumbNail = commenterThumbNail;
	}

	public String getMomentUuid() {
		return momentUuid;
	}

	public void setMomentUuid(String momentUuid) {
		this.momentUuid = momentUuid;
	}

	public String getMomentOwner() {
		return momentOwner;
	}

	public void setMomentOwner(String momentOwner) {
		this.momentOwner = momentOwner;
	}
}
