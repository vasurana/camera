package com.xvidia.cabi.utils;

import java.io.File;
import java.util.ArrayList;

/**
 * This class manages manipulation of files.
 * @author Ravi@Xvidia
 *	@since version 1.0
 */
public class FileManager {

	static ArrayList<String> fileList;
	
	
	/**
	 * This method deletes the folder directory.
	 * @param file directory to be deleted
	 * @return boolean value true is successfully deleted else false
	 */
	public static boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));
				
			}
	    }

	    // The directory is now empty so delete it
	    return dir.delete();
	}
	
	
	
	

	
}
